<?php

use Fvgestao\Api\FactoryClient;
use Fvgestao\Tests\TestCase;

class IdpTest extends TestCase
{
    public function testCriarUsuario()
    {
        $usuario = $this->idp->criarUsuario([
            'email' => uniqid() . '@teste.com',
            'senha' => '123456789',
            'dados' => ['nome' => 'a']
        ]);

        $this->assertEquals(201, $usuario['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'email', 'status', 'data_cadastro'], $usuario['data']);

    }
}
