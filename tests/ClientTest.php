<?php

use Fvgestao\Api\FactoryClient;
use Fvgestao\Tests\TestCase;

class ClientTest extends TestCase
{
    public function testFactory()
    {
        $config = [
            'idp' => [
                'version'       => '1',
                'client_id'     => '1',
                'client_secret' => 'teste',
                'endpoint'      => 'http://localhost:8000'
            ]
        ];

        $factory = new FactoryClient($config);

        $this->assertInstanceOf(FactoryClient::class, $factory);
    }
}
