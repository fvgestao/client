<?php
namespace Fvgestao\Tests;

use Fvgestao\Api\FactoryClient;

class TestCase extends \PHPUnit_Framework_TestCase
{
    protected $acl;
    protected $idp;
    protected $enterprise;

    public function seeJsonStructure(array $structure = null, $responseData = null)
    {
        if (is_null($structure)) {
            return $this->seeJson();
        }

        if (! $responseData) {
            return $this->seeJson();
        }

        foreach ($structure as $key => $value) {
            if (is_array($value) && $key === '*') {
                $this->assertInternalType('array', $responseData);

                foreach ($responseData as $responseDataItem) {
                    $this->seeJsonStructure($structure['*'], $responseDataItem);
                }
            } elseif (is_array($value)) {
                $this->assertArrayHasKey($key, $responseData);
                $this->seeJsonStructure($structure[$key], $responseData[$key]);
            } else {
                $this->assertArrayHasKey($value, $responseData);
            }
        }

        return $this;
    }

    private function seeJson(array $data = null, $negate = false)
    {
        if (is_null($data)) {
            $this->assertJson(
                $this->response->getContent(), "JSON was not returned from [{$this->currentUri}]."
            );

            return $this;
        }

        return $this->seeJsonContains($data, $negate);
    }

    public function setUp()
    {
        $config = [
            'idp' => [
                'version'       => '1',
                'client_id'     => getenv('CLIENT_ID'),
                'client_secret' => getenv('CLIENT_SECRET'),
                'endpoint'      => getenv('IDP_HOST')
            ],
            // 'acl' => [
            //     'version'       => '1',
            //     'client_id'     => getenv('CLIENT_ID'),
            //     'client_secret' => getenv('CLIENT_SECRET'),
            //     'endpoint'      => 'http://localhost:8001'
            // ],
            'enterprise' => [
                'version'       => '1',
                'client_id'     => getenv('CLIENT_ID'),
                'client_secret' => getenv('CLIENT_SECRET'),
                'endpoint'      => getenv('ENTERPRISE_HOST')
            ]
        ];

        $factory = new FactoryClient($config);

        $this->idp        = $factory->get('idp');
        // $this->acl        = $factory->get('acl');
        $this->enterprise = $factory->get('enterprise');
    }
}
