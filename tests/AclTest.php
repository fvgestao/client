<?php

use Fvgestao\Api\FactoryClient;
use Fvgestao\Tests\TestCase;

class AclTest extends TestCase
{
    // public function testCriarRecurso()
    //	{
    //		$recurso = $this->acl->criarRecurso([
    //            'nome' => 'usuario.listar' . uniqid()
    //        ]);
    //
    //        $this->assertEquals(201, $recurso['@metadata']['statusCode']);
    //
    //        return $recurso['data'];
    //	}
    //
    //    /**
    //     * @depends testCriarRecurso
    //     */
    //    public function testlistarRecursos()
    //    {
    //		$result = $this->acl->listarRecursos();
    //
    //        $structure = [
    //            '*' => ['id', 'nome']
    //        ];
    //
    //        $this->seeJsonStructure($structure, $result['data']);
    //
    //        $this->assertEquals(200, $result['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarRecurso
    //     */
    //    public function testVisualizarRecurso($recurso)
    //    {
    //        $result = $this->acl->visualizarRecurso([
    //            'id' => $recurso['id']
    //        ]);
    //
    //        $structure = ['id', 'nome'];
    //
    //        $this->seeJsonStructure($structure, $result['data']);
    //
    //        $this->assertEquals(200, $result['@metadata']['statusCode']);
    //    }
    //
    //    public function testAlterarRecurso()
    //    {
    //        $result = $this->acl->alterarRecurso([
    //            'id'   => 1,
    //            'nome' => 'recurso.listar'
    //        ]);
    //
    //        $this->assertEquals(200, $result['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarRecurso
    //     */
    //    public function testExluirRecurso($recurso)
    //    {
    //        $result = $this->acl->excluirRecurso([
    //            'id' => $recurso['id']
    //        ]);
    //
    //        $this->assertEquals(204, $result['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarEmpresaSistema
    //     */
    //    public function testCriarPerfil($data)
    //    {
    //        $perfil = $this->acl->criarPerfil([
    //            'nome'       => 'Administrador',
    //            'empresa_id' => $data['empresa_id'],
    //            'sistema_id' => $data['sistema_id']
    //        ]);
    //
    //        $this->assertEquals(201, $perfil['@metadata']['statusCode']);
    //        $this->seeJsonStructure(['id', 'nome', 'sistema_id', 'empresa_id'], $perfil['data']);
    //
    //        return $perfil['data'];
    //    }
    //
    //    /**
    //     * @depends testCriarPerfil
    //     */
    //    public function testListarPerfis()
    //    {
    //        $perfis = $this->acl->listarPerfis();
    //
    //        $this->assertEquals(200, $perfis['@metadata']['statusCode']);
    //    }
    //
    //    public function testAlterarPerfil()
    //    {
    //        $perfil = $this->acl->alterarPerfil([
    //            'id'   => 2,
    //            'nome' => 'Adminstrador Master'
    //        ]);
    //
    //        $this->assertEquals(200, $perfil['@metadata']['statusCode']);
    //    }
    //
    //    public function testExcluirPerfil()
    //    {
    //        //colocar dinamico apos criar todas as rotas do idp
    //        $perfil = $this->acl->criarPerfil([
    //            'nome'       => 'Administrador1',
    //            'empresa_id' => 'd3e13260-96fb-11e6-8f9b-15c525cc5e91',
    //            'sistema_id' => 31
    //        ]);
    //
    //        $result = $this->acl->excluirPerfil([
    //            'id' => $perfil['data']['id'],
    //            'empresa_id' => 'd3e13260-96fb-11e6-8f9b-15c525cc5e91',
    //            'sistema_id' => 31
    //        ]);
    //
    //        $this->assertEquals(204, $result['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarPerfil
    //     */
    //    public function testAdicionarRecursoNoPerfil($perfil)
    //    {
    //        $recurso = $this->acl->criarRecurso([
    //            'nome' => 'perfil.recurso'
    //        ]);
    //
    //        $recursoDoPerfil = $this->acl->adicionarRecursoNoPerfil([
    //            'perfil_id'  => $perfil['id'],
    //            'recurso_id' => $recurso['data']['id']
    //        ]);
    //
    //        $this->assertEquals(201, $recursoDoPerfil['@metadata']['statusCode']);
    //    }
    //
    //    public function testlistarRecursosDoPerfil()
    //    {
    //        $perfis = $this->acl->listarPerfis();
    //
    //        $recursosDoPerfil = $this->acl->listarRecursosDoPerfil([
    //            'perfil_id'  => current($perfis['data'])['id']
    //        ]);
    //
    //        $structure = [
    //            '*' => ['id', 'nome']
    //        ];
    //
    //        $this->seeJsonStructure($structure, $recursosDoPerfil['data']);
    //
    //        $this->assertEquals(200, $recursosDoPerfil['@metadata']['statusCode']);
    //    }
    //
    //    public function testExcluirRecursoDoPerfil()
    //    {
    //        $perfis   = $this->acl->listarPerfis();
    //        $recursos = $this->acl->listarRecursos();
    //
    //        $excluido = $this->acl->excluirRecursoDoPerfil([
    //            'perfil_id'  => current($perfis['data'])['id'],
    //            'recurso_id' => end($recursos['data'])['id']
    //        ]);
    //
    //        $this->assertEquals(204, $excluido['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarEmpresaSistema
    //     */
    //    public function testAdicionarRecursoNoUsuario($colaborador, $data)
    //    {
    //        $recurso = $this->acl->criarRecurso([
    //            'nome' => 'teste.' . uniqid()
    //        ]);
    //
    //        $adicionado = $this->acl->adicionarRecursoNoUsuario([
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'empresa_id' => $data['empresa_id'],
    //            'sistema_id' => $data['sistema_id'],
    //            'recurso_id' => $recurso['data']['id']
    //        ]);
    //
    //        $this->assertEquals(201, $adicionado['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarEmpresaSistema
    //     */
    //    public function testListarRecursosDoUsuario($colaborador, $data)
    //    {
    //        $recursosDoUsuario = $this->acl->listarRecursosDoUsuario([
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'empresa_id' => $data['empresa_id'],
    //            'sistema_id' => $data['sistema_id']
    //        ]);
    //
    //        $structure = [
    //            '*' => ['id', 'nome']
    //        ];
    //
    //        $this->seeJsonStructure($structure, $recursosDoUsuario['data']);
    //
    //        $this->assertEquals(200, $recursosDoUsuario['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarPerfil
    //     */
    //    public function testAdicionarPerfilNoUsuario($colaborador, $perfil)
    //    {
    //        $adicionado = $this->acl->adicionarPerfilNoUsuario([
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'perfil_id'  => $perfil['id']
    //        ]);
    //
    //        $this->assertEquals(201, $adicionado['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testAdicionarPerfilNoUsuario
    //     */
    //    public function testListarPerfisDoUsuario($colaborador)
    //    {
    //        $perfis = $this->acl->listarPerfisDoUsuario([
    //            'usuario_id' => $colaborador['usuario_id']
    //        ]);
    //
    //        $structure = [
    //            '*' => ['id', 'nome', 'empresa_id', 'sistema_id']
    //        ];
    //
    //        $this->seeJsonStructure($structure, $perfis['data']);
    //        $this->assertEquals(200, $perfis['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarPerfil
    //     * @depends testAdicionarPerfilNoUsuario
    //     */
    //    public function testExcluirPerfisDoUsuario($colaborador, $perfil)
    //    {
    //        $excluido = $this->acl->excluirPerfilDoUsuario([
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'perfil_id'  => $perfil['id']
    //        ]);
    //
    //        $this->assertEquals(204, $excluido['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarEmpresaSistema
    //     */
    //    public function testExcluirRecursoDoUsuario($colaborador, $data)
    //    {
    //        $recurso = $this->acl->criarRecurso([
    //            'nome' => 'teste.' . uniqid()
    //        ]);
    //
    //        $params = [
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'empresa_id' => $data['empresa_id'],
    //            'sistema_id' => $data['sistema_id'],
    //            'recurso_id' => $recurso['data']['id']
    //        ];
    //
    //        $this->acl->adicionarRecursoNoUsuario($params);
    //
    //        $excluido = $this->acl->excluirRecursoDoUsuario($params);
    //
    //        $this->assertEquals(204, $excluido['@metadata']['statusCode']);
    //    }
    //
    //    /**
    //     * @depends testCriarColaborador
    //     * @depends testCriarPerfil
    //     */
    //    public function testListarRecursosDoUsuarioPerfil($colaborador, $perfil)
    //    {
    //        $recursos = $this->acl->listarRecursosDoUsuarioPerfil([
    //            'usuario_id' => $colaborador['usuario_id'],
    //            'perfil_id'  => $perfil['id']
    //        ]);
    //
    //        $this->seeJsonStructure(['*' => []], $recursos['data']);
    //        $this->assertEquals(200, $recursos['@metadata']['statusCode']);
    //    }
}
