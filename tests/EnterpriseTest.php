<?php

use Fvgestao\Api\FactoryClient;
use Fvgestao\Tests\TestCase;

class EnterpriseTest extends TestCase
{
    public function testCriarUsuario()
    {
        $usuario = $this->idp->criarUsuario([
            'email' => uniqid() . '@teste.com',
            'senha' => '123456789',
            'dados' => ['nome' => 'a']
        ]);

        $this->assertEquals(201, $usuario['@metadata']['statusCode']);

        return $usuario['data'];
    }

    /************************** Empresa ***************************************/

    public function testCriarEmpresa()
    {
        $empresa = $this->enterprise->criarEmpresa([
            'nome_fantasia' => 'Empresa '. uniqid()
        ]);

        $this->assertEquals(201, $empresa['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'data_cadastro'], $empresa['data']);

        return $empresa['data'];
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testCriarEmpresaSemParametroObrigratorio()
    {
        $this->enterprise->criarEmpresa(); // required nome_fantasia
    }

    /**
     * @depends testCriarEmpresa
     */
    public function testAlterarEmpresa($empresa)
    {
        $empresa['nome_fantasia'] = "Empresa 1000";
        $result = $this->enterprise->AlterarEmpresa($empresa);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testAlterarEmpresaSemParametroObrigratorio()
    {
        $this->enterprise->alterarEmpresa();
    }

    /**
     * @depends testCriarEmpresa
     */
    public function testVisualizarEmpresa($empresa)
    {
        $result = $this->enterprise->visualizarEmpresa($empresa);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'data_cadastro'], $result['data']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarEmpresaSemParametroObrigratorio()
    {
        $this->enterprise->alterarEmpresa();
    }

    /********************* Fim de Empresa *************************************/

    /********************** Colaborador ***************************************/

    /**
    * @group Colaborador
    * @depends testCriarUsuario
    * @depends testCriarEmpresa
    */
    public function testCriarColaborador($usuario, $empresa)
    {
        $colaborador = $this->enterprise->criarColaborador([
            'usuario_id' => $usuario['id'],
            'empresa_id' => $empresa['id']
        ]);

        $this->assertEquals(201, $colaborador['@metadata']['statusCode']);
        $this->seeJsonStructure(
            ['id', 'usuario_id', 'empresa_id'],
            $colaborador['data']
        );

        return $colaborador['data'];
    }

    /**
    * @group Colaborador
    * @expectedException InvalidArgumentException
    */
    public function testCriarColaboradorSemParametroObrigratorio()
    {
        $this->enterprise->criarColaborador(); // required id_empresa e id_usuario;
    }

    /**
    * @group Colaborador
    * @depends testCriarColaborador
    */
    public function testVisualizarColaborador($colaborador)
    {
        $result = $this->enterprise->visualizarColaborador($colaborador);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'usuario_id'], $result['data']);
    }

    /**
    * @group Colaborador
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarColaboradorSemParametroObrigatorio()
    {
        $this->enterprise->visualizarColaborador();
    }

    /**
    * @group Colaborador
    * @expectedException Api\Exception\ApiException
    */
    public function testVisualizarColaboradorComParametroErrado()
    {
      $this->enterprise->visualizarColaborador([
            'id' => 1000 // não existe;
        ]);
    }
    /**
    * @group Colaborador
    * @depends testCriarColaborador
    */
    public function testListarColaboradores($colaborador)
    {
        $result = $this->enterprise->listarColaboradores([
            'empresa_id' => $colaborador['empresa_id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @group Colaborador
    * @expectedException InvalidArgumentException
    */
    public function testListarColaboradorSemParametroObrigratorio()
    {
        $this->enterprise->listarColaborador(); // required id_empresa;
    }

    /**
    * @group Colaborador
    * @expectedException InvalidArgumentException
    */
    public function testListarColaboradorComParametroErrado()
    {
        $this->enterprise->listarColaborador([
                'id' => 1244  // não existe
                ]);
    }

    /******************** Fim Colaborador ************************************/

    /******************** EmpresaSistema ************************************/
    /**
    * @group EmpresaSistema
    * @expectedException InvalidArgumentException
    */
    public function testCriarEmpresaSistemaSemParametroObrigratorio()
    {
        $this->enterprise->criarEmpresaSistema(); // required [empresa_id] [sistema_id]
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testCriarEmpresaSistemaComDadosErrados()
    {

        $result = $this->enterprise->criarEmpresaSistema([
            'empresa_id' => 800,
            'sistema_id' => 30,
        ]);

        $this->assertEquals(201, $result['@metadata']['statusCode']);

    }

    /**
    * @depends testCriarEmpresa
    */
    public function testCriarEmpresaSistema($empresa)
    {
        $sistemaId = getenv('CLIENT_ID');

        $result = $this->enterprise->criarEmpresaSistema([
            'empresa_id' => $empresa['id'],
            'sistema_id' => $sistemaId,
        ]);

        $this->assertEquals(201, $result['@metadata']['statusCode']);

        return [
            'empresa_id' => $empresa['id'],
            'sistema_id' => $sistemaId
        ];
    }
    /******************** Fim EmpresaSistema ************************************/

    /*************************** Plano de Acao ********************************/
    /**
    * @depends testCriarColaborador
    */
    public function testCriarPlanoDeAcao($colaborador)
    {
        $date = new \DateTime();

        $planoDeAcao = $this->enterprise->criarPlanoDeAcao([
            'responsavel_id' => $colaborador['id'],
            'nome'           => 'Plano de Ação ' .uniqid(),
            'prazo'          => $date->format('Y-m-d'),
            'objetivo'       => 'Objetivo ' .uniqid(),
            'local'          => 'Local ' .uniqid(),
            'custo'          => 200.0,
        ]);

        $this->assertEquals(201, $planoDeAcao['@metadata']['statusCode']);

        return $planoDeAcao['data'];
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testCriarPlanoDeAcaoComDadosErrados()
    {
        $date = new \DateTime();

        $planoDeAcao = $this->enterprise->criarPlanoDeAcao([
            'responsavel_id' => 10,
            'nome'           => 'Plano de Ação ' .uniqid(),
            'prazo'          => $date->format('Y-m-d'),
            'objetivo'       => 'Objetivo ' .uniqid(),
            'local'          => 'Local ' .uniqid(),
            'custo'          => 35000.0,
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testCriarPlanoDeAcaoSemParametroObrigratorio()
    {
        $this->enterprise->criarPlanoDeAcao(); // required nome_fantasia
    }

    /**
    * @depends testCriarColaborador
    */
    public function testListarPlanoDeAcoesDaEmpresa($colaborador)
    {
        $result = $this->enterprise->ListarPlanoDeAcoesDaEmpresa([
            'empresa_id' => $colaborador['empresa_id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarPlanoDeAcoesDaEmpresaComDadosErrados()
    {
        $this->enterprise->listarPlanoDeAcoesDaEmpresa([
            'empresa_id' => 100 // empresa não existe;
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarPlanoDeAcoesDaEmpresaSemParametroObrigratorio()
    {
        $this->enterprise->listarPlanoDeAcoesDaEmpresa(); // required empresa_id
    }

    /**
    * @depends testCriarColaborador
    */
    public function testListarPlanoDeAcaoDoColaborador($colaborador)
    {
        $result = $this->enterprise->listarPlanoDeAcaoDoColaborador([
            'responsavel_id' => $colaborador['id']
        ]);

        $this->seeJsonStructure(
            ['*' =>
                ['id', 'responsavel_id', 'nome', 'prazo', 'objetivo', 'local', 'custo']
            ],
            $result['data']
        );

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarPlanoDeAcoesDoColaboradorComDadosErrados()
    {
        $this->enterprise->listarPlanoDeAcaoDoColaborador([
            'responsavel_id' => 1000 // empresa não existe;
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarPlanoDeAcoesDoColaboradorSemParametroObrigratorio()
    {
        $this->enterprise->listarPlanoDeAcaoDoColaborador(); // required colaborador_id
    }

    /*********************** Tarefas do Plano de Ação *************************/
    /**
    * @depends testCriarPlanoDeAcao
    */
    public function testListarTarefasDoPlanoDeAcao($planoDeAcao)
    {
        $result = $this->enterprise->listarTarefasDoPlanoDeAcao([
            'plano_id' => $planoDeAcao['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarTarefasDoPlanoDeAcaoComDadosErrados()
    {
        $this->enterprise->listarTarefasDoPlanoDeAcao([
            'plano_id' => 1000 // empresa não existe;
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarTarefasDoPlanoDeAcaoSemParametroObrigratorio()
    {
        $this->enterprise->listarTarefasDoPlanoDeAcao(); // required planodeacao_id
    }

    /********************** Tarefas do Plano de Ação **************************/

    /**
    * @depends testCriarPlanoDeAcao
    */
    public function testVisualizarPlanoDeAcao($planoDeAcao)
    {
        $result = $this->enterprise->visualizarPlanoDeAcao([
            'id' => $planoDeAcao['id']
        ]);

        $this->seeJsonStructure(
            ['id', 'responsavel_id', 'nome', 'prazo', 'objetivo', 'local', 'custo'],
            $result['data']
        );

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testVisualizarPlanoDeAcaoComDadosErrados()
    {
        $this->enterprise->visualizarPlanoDeAcao([
            'id' => 1000 // plano de ação não existe;
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarPlanoDeAcaoSemParametroObrigratorio()
    {
        $this->enterprise->visualizarPlanoDeAcao(); // required id do plano de ação
    }

    /**
    * @depends testCriarPlanoDeAcao
    */
    public function testAlterarPlanoDeAcao($planoDeAcao)
    {
        $planoDeAcao['objetivo'] = "Colocar a FVgestao no top".

        $result = $this->enterprise->alterarPlanoDeAcao($planoDeAcao);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testAlterarPlanoDeAcaoComDadoErrado()
    {
        $this->enterprise->alterarPlanoDeAcao([
            'id' => 10000, // plando de ação não existe
            'objetivo' => "fazer o teste falhar"
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testAlterarPlanoDeAcaoSemParametroObrigratorio()
    {
        $this->enterprise->alterarPlanoDeAcao(); // required id do plano de ação
    }

    /**
    * @depends testCriarColaborador
    */
    public function testExcluirPlanoDeAcao($colaborador)
    {
        $date = new \DateTime();

        $planoDeAcao = $this->enterprise->criarPlanoDeAcao([
            'responsavel_id' => $colaborador['id'],
            'nome'           => 'Plano de Ação ' .uniqid(),
            'prazo'          => $date->format('Y-m-d'),
            'objetivo'       => 'Objetivo ' .uniqid(),
            'local'          => 'Local ' .uniqid(),
            'custo'          => 200.0,
        ]);

        $result = $this->enterprise->excluirPlanoDeAcao([
            'id' => $planoDeAcao['data']['id']
        ]);

        $this->assertEquals(204, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testExcluirPlanoDeAcaoComDadoErrado()
    {
        $this->enterprise->excluirPlanoDeAcao([
            'id' => 10000, // plando de ação não existe
        ]);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testExcluirPlanoDeAcaoSemParametroObrigratorio()
    {
        $this->enterprise->excluirPlanoDeAcao(); // required id do plano de ação
    }

    /*************************** Plano de Acao ********************************/

    /**
    * @depends testCriarColaborador
    */
    public function testListarColaboradoresDoUsuario($colaborador)
    {
        $result = $this->enterprise->listarColaboradoresDoUsuario([
            'usuario_id' => $colaborador['usuario_id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    public function testListarPrioridadesDaTarefa()
    {
        $prioridades = $this->enterprise->listarPrioridadeDaTarefa();

        $this->assertEquals(200, $prioridades['@metadata']['statusCode']);

        return $prioridades['data'];
    }

    /*************************** Tarefa ********************************/

    /**
    * @depends testCriarPlanoDeAcao
    * @depends testListarPrioridadesDaTarefa
    */
    public function testCriarTarefa($planoDeAcao, $prioridades)
    {
        $date = new DateTime();
        $prioridade = current($prioridades);

        $tarefa = $this->enterprise->criarTarefa([
            'nome'     => "Tarefinha de casa",
            'plano_id' => $planoDeAcao['id'],
            'executante_id' => $planoDeAcao['responsavel_id'],
            'prioridade_id' => $prioridade['id'],
            'descricao' => "Descrição " . uniqid(),
            'data_inicio' => $date->format('Y-m-d'),
            'data_prazo' => $date->format('Y-m-d')
        ]);

        $this->assertEquals(201, $tarefa['@metadata']['statusCode']);

        return $tarefa['data'];
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testCriarTarefaSemParametroObrigatorio()
    {
        $this->enterprise->criarTarefa();
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testCriarTarefaComParametroErrado()
    {
        $this->enterprise->criarTarefa([
            'plano_id' => 1000,
            ]);
    }

    /**
    * @depends testCriarTarefa
    */
    public function testVisualizarTarefa($tarefa)
    {
        $result = $this->enterprise->visualizarTarefa([
            'id' => $tarefa['id']
        ]);

        $this->seeJsonStructure([
            'id', 'executante_id', 'prioridade_id',
            'plano_id', 'descricao', 'data_inicio',
            'data_prazo'
        ], $result['data']);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarTarefaSemParametroObrigatorio()
    {
        $this->enterprise->visualizarTarefa();
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarTarefaComParametroErrado()
    {
        $this->enterprise->visualizarTarefa([
            'plano_id' => 10000,
            ]);
    }

    /**
    * @depends testCriarTarefa
    */
    public function testAlterarTarefa($tarefa)
    {
        $result = $this->enterprise->alterarTarefa([
            'id'        => $tarefa['id'],
            'descricao' => "ALTERAÇÃO" . uniqid()
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testAlterarTarefaSemParametroObrigatorio()
    {
        $this->enterprise->alterarTarefa();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testAlterarTarefaComParametroErrado()
    {
        $this->enterprise->alterarTarefa([
            'id'        => 1123,
            'descricao' => "Alterando Errado",
            ]);
    }

    /**
    * @depends testCriarTarefa
    */
    public function testExcluirTarefa($tarefa)
    {
        
        $result = $this->enterprise->excluirTarefa([
            'id' => $tarefa['id']
        ]);

        $this->assertEquals(204, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testExcluirTarefaSemParametroObrigatorio()
    {
        $this->enterprise->excluirTarefa();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testExcluirTarefaComParametroErrado()
    {
        $this->enterprise->excluirTarefa([
            'id'    => 154648,
            ]);
    }

    /*************************** FIM TAREFA ********************************/

    /**
    * @depends testCriarColaborador
    */
    public function testListarEquipesDoColaborador($colaborador)
    {
        $result = $this->enterprise->listarEquipesDoColaborador([
            'colaborador_id'=> $colaborador['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }
    
    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarEquipesDoColaboradorSemParametroObrigatorio()
    {
        $this->enterprise->listarEquipesDoColaborador();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarEquipesDoColaboradorComParametroErrado()
    {
        $this->enterprise->listarEquipesDoColaborador([
            'colaborador_id' => 156,
            ]);
    }

    /************************** Cargo *****************************************/
    /**
    * @depends testCriarEmpresa
    */
    public function testCriarCargo($empresa)
    {
        $cargo = $this->enterprise->criarCargo([
            'empresa_id' => $empresa['id'],
            'nome'       => $empresa['nome_fantasia'],
            'situacao'   => 'ATIVO'
        ]);

        $this->assertEquals(200, $cargo['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'empresa_id', 'nome', 'situacao'], $cargo['data']);

        return $cargo['data'];
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testCriarCargoSemParametroObrigratorio()
    {
        $this->enterprise->criarCargo();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testCriarCargoComEmpresaInexistente()
    {
        $this->enterprise->criarCargo([
            'empresa_id' => 100, // Empresa não existe;
            'nome' => 'empresa teste',
            'situacao' => 'ATIVO'
        ]);
    }

    /**
    * @depends testCriarEmpresa
    */
    public function testListarCargos($empresa)
    {
        $result = $this->enterprise->listarCargos([
            'empresa_id' => $empresa['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarCargoSemParametroObrigratorio()
    {
        $result = $this->enterprise->listarCargos();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarCargoComEmpresaInexistente()
    {
        $result = $this->enterprise->listarCargos([
            'empresa_id' => 100 // Empresa não existe;
        ]);
    }

    /**
    * @depends testCriarCargo
    */
    public function testVisualizarCargo($cargo)
    {
        $result = $this->enterprise->visualizarCargo([
            'id' => $cargo['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
        $this->seeJsonStructure(
            ['id', 'empresa_id', 'nome', 'situacao'],
            $result['data']
        );
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarCargoSemParametroObrigatorio()
    {
        $this->enterprise->visualizarCargo([
            'id' => NULL
        ]);
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testVisualizarCargoComParametroErrado()
    {
        $this->enterprise->visualizarCargo([
            'id' => 100 // Cargo não existe
        ]);
    }
    /************************** Cargo *****************************************/

    /************************** Equipe *****************************************/
    /**
    * @depends testCriarEmpresa
    * "required": ["empresa_id", "nome", "situacao"],
    */
    public function testCriarEquipe($empresa)
    {
        $equipe = $this->enterprise->criarEquipe([
            'empresa_id' => $empresa['id'],
            'nome'       => 'Equipe 1',
            'situacao'   => 'INATIVO'
        ]);

        $this->assertEquals(201, $equipe['@metadata']['statusCode']);
        $this->seeJsonStructure(['id', 'empresa_id', 'nome', 'situacao'], $equipe['data']);

        return $equipe['data'];
    }

    /**
    * @expectedException InvalidArgumentException
    * "required": ["empresa_id", "nome", "situacao"],
    */
    public function testCriarEquipeSemParametroObrigratorio()
    {
        $this->enterprise->criarEquipe();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testCriarEquipeComEmpresaInexistente()
    {
        $this->enterprise->criarEquipe([
            'empresa_id' => 100, // Empresa não existe;
            'nome' => 'equipe teste',
            'situacao' => 'ATIVO'
        ]);
    }

    /**
     * @depends testCriarEquipe
     */
    public function testAlterarEquipe($equipe)
    {
        $equipe['nome'] = "Equipe Nota 10";
        $result = $this->enterprise->alterarEquipe($equipe);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
     * @expectedException InvalidArgumentException
     * "required": ["id"]
     */
    public function testAlterarEquipeSemParametroRequerido()
    {
        $this->enterprise->alterarEquipe();
    }

    /**
     * @expectedException Api\Exception\ApiException
     * "required": ["id"]
     */
    public function testAlterarEquipeComParametroErrado()
    {
        $this->enterprise->alterarEquipe([
            "id" => 100000 // Equipe não existe;
        ]);
    }

    /**
    * @depends testCriarEquipe
    */
    public function testVisualizarEquipe($equipe)
    {
        $result = $this->enterprise->visualizarEquipe([
            'id' => $equipe['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
        $this->seeJsonStructure(
            ['id', 'empresa_id', 'nome', 'situacao'],
            $result['data']
        );
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testVisualizarEquipeSemParametroObrigatorio()
    {
        $this->enterprise->visualizarEquipe();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testVisualizarEquipeComParametroErrado()
    {
        $this->enterprise->visualizarEquipe([
            'id' => 100 // Equipe não existe
        ]);
    }

    /**
    * @depends testCriarEmpresa
    */
    public function testListarEquipes($empresa)
    {
        $result = $this->enterprise->listarEquipes([
            'empresa_id' => $empresa['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function testListarEquipesSemParametroObrigratorio()
    {
        $result = $this->enterprise->listarEquipes();
    }

    /**
    * @expectedException Api\Exception\ApiException
    */
    public function testListarEquipesComEmpresaInexistente()
    {
        $result = $this->enterprise->listarEquipes([
            'empresa_id' => 1000000 // Empresa não existe;
        ]);
    }

    /**
     * @depends testCriarEquipe
     */
    public function testAdicionarEquipeParente($equipe)
    {
        $equipePai = $this->enterprise->criarEquipe([
            'empresa_id' => '07a31f90-c600-11e6-aabc-89d14ab6b23c',
            'nome'       => 'Equipe Pai',
            'situacao'   => 'ATIVO'
        ]);

        $result = $this->enterprise->adicionarEquipeParente([
            'equipe_id' => $equipe['id'],
            'pai_id'=> $equipePai['data']['id']
        ]);

        $this->assertEquals(201, $result['@metadata']['statusCode']);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testAdicionarEquipeParenteSemParametro()
    {
        $result = $this->enterprise->adicionarEquipeParente();
    }

    /**
     * @expectedException Api\Exception\ApiException
     */
    public function testAdicionarEquipeParenteComParametroErrado()
    {
        $result = $this->enterprise->adicionarEquipeParente([
            'equipe_id' => 1,
            'pai_id' => 2
        ]);
    }

    /**
     * @depends testCriarEquipe
     */
    public function testDefinirEquipeComoRoot($equipe)
    {
        $result = $this->enterprise->definirEquipeComoRoot([
            'equipe_id' => $equipe['id'],
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testDefinirEquipeComoRootSemParametro()
    {
        $result = $this->enterprise->definirEquipeComoRoot();
    }

    /**
     * @expectedException Api\Exception\ApiException
     */
    public function testDefinirEquipeComoRootComParametroErrado()
    {
        $result = $this->enterprise->definirEquipeComoRoot([
            'equipe_id' => 1,
        ]);
    }

    public function testSubordinado()
    {
        $result = $this->enterprise->subordinado([
            'colaborador_id' => '04620110-cb80-11e6-bd5d-7f6fd6b6cb9a',
            'superior_id'=> '031373b0-cb85-11e6-b4c0-a7f2a6895a80'
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testSubordinadoSemParametro()
    {
        $result = $this->enterprise->subordinado();
    }

    /**
     * @expectedException Api\Exception\ApiException
     */
    public function testSubordinadoComParametroErrado()
    {
        $result = $this->enterprise->subordinado([
            'colaborador_id' => 1,
            'superior_id' => 2
        ]);
    }

    /**
     * @depends testCriarColaborador
     */
    public function testListarNaoEquipesDoColaborador($colaborador)
    {
        $result = $this->enterprise->listarNaoEquipesDoColaborador([
            'colaborador_id' => $colaborador['id']
        ]);

        $this->assertEquals(200, $result['@metadata']['statusCode']);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testListarNaoEquipesDoColaboradorSemParametro()
    {
        $result = $this->enterprise->listarNaoEquipesDoColaborador();
    }

    /**
     * @expectedException Api\Exception\ApiException
     */
    public function testListarNaoEquipesDoColaboradorComParametroErrado()
    {
        $result = $this->enterprise->listarNaoEquipesDoColaborador([
            'colaborador_id' => 1,
            'superior_id' => 2
        ]);
    }
    /************************** Equipe *****************************************/
}
