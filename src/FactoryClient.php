<?php

namespace Fvgestao\Api;

use Fvgestao\Api\Exceptions\InvalidClientException;
use Fvgestao\Api\Exceptions\InvalidConfigException;

class FactoryClient
{
    protected $services = [
        'idp'        => \Fvgestao\Api\Clients\IdpClient::class,
        'acl'        => \Fvgestao\Api\Clients\AclClient::class,
        'email'      => \Fvgestao\Api\Clients\EmailClient::class,
        'enterprise' => \Fvgestao\Api\Clients\EnterpriseClient::class
    ];

    protected $config = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function get($name)
    {
        if (! isset($this->services[$name])) {
            throw new InvalidClientException(sprintf("O Client %s não foi encontrada", $name));
        }

        if (! isset($this->config[$name])) {
            throw new InvalidConfigException(sprintf("A configuração %s não foi encontrada", $name));
        }

        $client = $this->services[$name];

        return new $client($this->config[$name]);
    }
}