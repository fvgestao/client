<?php

namespace Fvgestao\Api\Exceptions;

use Exception as BaseException;

class InvalidClientException extends BaseException
{
    protected $code = 404;
     
    public function __construct($message = null)
    {
        if (!$message) {
            $message = 'Client não encontrado.';
        }
        
        parent::__construct($message, $this->getCode());
    }
}
