<?php

namespace Fvgestao\Api\Exceptions;

use Exception as BaseException;

class InvalidConfigException extends BaseException
{
    protected $code = 404;
     
    public function __construct($message = null)
    {
        if (!$message) {
            $message = 'Não foi encontrada a configuração';
        }
        
        parent::__construct($message, $this->getCode());
    }
}
