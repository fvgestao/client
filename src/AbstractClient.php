<?php

namespace Fvgestao\Api;

use Psr\Http\Message\RequestInterface;
use Api\Client as ApiClient;
use Api\CommandInterface;

abstract class AbstractClient extends ApiClient
{
    protected $token;

    public function __construct(array $args)
    {
        if (! isset($args['modelsDir'])) {
            $args['modelsDir'] = __DIR__ . '/../data';
        }

        if (! (isset($args['client_id']) || isset($args['client_secret']))) {
            throw new \InvalidArgumentException('O client_id e o client_secret devem ser informados');
        }

        parent::__construct($args);

        $tokenResponse = $this->obterToken([
            'client_id' => $args['client_id'],
            'client_secret' => $args['client_secret'],
            'grant_type' => 'client_credentials'
        ]);

        $token = $tokenResponse->get('access_token');

        $stack = $this->getHandlerList();

        $stack->appendBuild(function (callable $handler) use ($token) {
            return function (CommandInterface $command, RequestInterface $request) use ($handler, $token) {
                $request = $request->withHeader('Authorization', 'Bearer ' . $token);

                return $handler($command, $request);
            };
        });
    }
}